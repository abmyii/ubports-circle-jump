extends Node

signal start_game
signal a

var sound_buttons = {true: preload("res://assets/images/buttons/audioOn.png"),
					false: preload("res://assets/images/buttons/audioOff.png")}
var music_buttons = {true: preload("res://assets/images/buttons/musicOn.png"),
					false: preload("res://assets/images/buttons/musicOff.png")}
					
var current_screen = null

var highscore_colours = {
	0: Color(1, 0.84, 0),  # Gold
	1: Color(0.6, 0.6, 0.6),  # Silver
	2: Color(0.8, 0.5, 0.2)  # Bronze
}

func _ready():
	register_buttons()
	change_screen($TitleScreen)
	
func register_buttons():
	var buttons = get_tree().get_nodes_in_group("buttons")
	for button in buttons:
		button.connect("pressed", self, "_on_button_pressed", [button])
		match button.name:
			"Ads":
				if not Engine.has_singleton("AdMob"):
					button.hide()
				if settings.enable_ads:
					button.text = "Disable Ads"
				else:
					button.text = "Enable Ads"
			"Sound":
				button.texture_normal = sound_buttons[settings.enable_sound]
			"Music":
				button.texture_normal = music_buttons[settings.enable_music]

func _on_button_pressed(button):
	if settings.enable_sound:
		$Click.play()
	match button.name:
		"About":
			change_screen($AboutScreen)
		"Ads":
			settings.enable_ads = !settings.enable_ads
			if settings.enable_ads:
				button.text = "Disable Ads"
			else:
				button.text = "Enable Ads"
		"Home":
			change_screen($TitleScreen)
		"Play":
			change_screen(null)
			yield(get_tree().create_timer(0.5), "timeout")
			emit_signal("start_game")
		"Settings":
			change_screen($SettingsScreen)
		"Sound":
			settings.enable_sound = !settings.enable_sound
			button.texture_normal = sound_buttons[settings.enable_sound]
			settings.save_settings()
		"Music":
			settings.enable_music = !settings.enable_music
			button.texture_normal = music_buttons[settings.enable_music]
			settings.save_settings()

func change_screen(new_screen):
	if current_screen:
		current_screen.disappear()
		yield(current_screen.tween, "tween_completed")
	current_screen = new_screen
	if new_screen:
		current_screen.appear()
		yield(current_screen.tween, "tween_completed")

func game_over(score_index):
	var score_label = $GameOverScreen/MarginContainer/VBoxContainer/Score
	var score_box = $GameOverScreen/MarginContainer/VBoxContainer/Scores
	
	for child in score_box.get_children():
		score_box.remove_child(child)
	
	score_label.text = "Score: %s" % $"/root/Main".score
	var index = 0
	for highscore in $"/root/Main".highscores:
		if highscore:
			var highscore_label = score_label.duplicate()
			var font = highscore_label.get_font("font").duplicate()
			
			font.size = 25 - index
			
			highscore_label.add_font_override("font", font)
			
			# Highlight the player's new highscore in red
			if score_index == index:
				highscore_label.add_color_override("font_color", Color(1,0,0))
			elif index in highscore_colours:
				highscore_label.add_color_override("font_color", highscore_colours[index])

			highscore_label.text = str(index+1, '. ', highscore.name, ": " , highscore.score)
			score_box.add_child(highscore_label)
			
			index += 1

	change_screen($GameOverScreen)

func enter_name():
	change_screen($EnterNameScreen)
